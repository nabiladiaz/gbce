import React from 'react';
import { Link } from 'react-router-dom';

const HomePage = () => {
    return (
        <div className="jumbotron">
            <h1>Global Beverage Corporation Exchange Stock Market</h1>
            <h3><Link to="/stocks">GBCE Stocks</Link></h3>
            <h3><Link to="/trade">Trades</Link></h3>
            <h3><Link to="/about">About</Link></h3>
        </div>
    );
};

export default HomePage;
