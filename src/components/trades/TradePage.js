import React from 'react';
import StockApi from '../../mockAPI/mockStockApi';
import TradeApi from '../../mockAPI/mockTradeApi';

export default class TradePage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            operation: 'buy',
            quantity: '0',
            stocks: [],
            loaded: false,
            invalid: false,
            errors: {}
        }

        this.onChangeQuantity = this.onChangeQuantity.bind(this);
        this.trade = this.trade.bind(this);
    }

    onChangeQuantity(e, stock) {
        e.preventDefault();

        e.target.value > stock.quantity ?
            this.setState({ invalid: true }) :
            this.setState({ invalid: false, quantity: e.target.value });
    }

    trade(e, stock, type) {
        e.preventDefault();

        let trade = {
            stockName: stock.symbol,
            stockId: stock.id,
            price: stock.price,
            timestamp: new Date(),
            quantity: this.state.quantity,
            type: type
        };

        TradeApi.saveTrade(trade)
            .then(res => StockApi.updateStockQuantity(res))
            .then(res => {
                let stocks = this.state.stocks, stockIndex = -1;
                stockIndex = stocks.findIndex(elem => elem.id === res.id);
                stocks.splice(stockIndex, 1, res);
                this.setState(stocks);
            })
            .catch(error => {throw new Error(error)});
    }

    componentDidMount() {
        //Get stocks from endpoint, in this case the mockStockAPI
        StockApi.getAllStocks()
            .then(
                response => {
                    this.setState({
                        loaded: true,
                        stocks: response
                    });
                },
                error => {
                    this.setState({
                        error
                    });
                }
            )
    }

    render() {
        let stocks = this.state.stocks;

        if (!this.state.loaded) {
            return null
        } else {
            return (
                <div className="trade-ctnr">
                    <h2>Global Beverage Corporation Exchange Trades</h2>
                    <ul className="stock__list">
                        {stocks.map(stock =>
                            <li className="stock__elem" key={stock.id} data-name={stock.symbol}>
                                <form>
                                    <div className="metrics-ctnr">
                                        <span className="stock__metric">Available stocks: {stock.quantity}</span>
                                        <label htmlFor={name}>Stock Price</label>
                                        <span className="stock__metric">Price: {stock.price}</span>
                                        <input className="stock__qnty" type="text" onChange={e => this.onChangeQuantity(e, stock)} placeholder="Enter amount" />
                                        <button className="stock__btn" onClick={e => this.trade(e, stock, 'buy')}>Buy</button>
                                        <button className="stock__btn" onClick={e => this.trade(e, stock, 'sell')}>Sell</button>
                                    </div>
                                </form>
                            </li>
                        )}
                    </ul>
                </div>
            );
        }
    }
}
