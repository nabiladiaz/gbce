import React from 'react';
import StockApi from '../../mockAPI/mockStockApi';
import TradeApi from '../../mockAPI/mockTradeApi';
import { calculateDividendYield, calculatePERatio, calculateVolumeWeightedStockPrice, calculateAllShareIndex } from '../../utils/math';

export default class StockPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            stocks: [],
            currentPrice: 0,
            loaded: false,
            errors: {}
        }

        this.onChangePrice = this.onChangePrice.bind(this);
        this.calculate = this.calculate.bind(this);
        this.getVolumeWeightedStockPrice = this.getVolumeWeightedStockPrice.bind(this);
        this.getAllShareIndex = this.getAllShareIndex.bind(this);
    }

    componentDidMount() {
        //Get stocks from endpoint, in this case the mockStockAPI
        StockApi.getAllStocks()
            .then(
                response => {
                    this.setState({
                        loaded: true,
                        stocks: response
                    });
                },
                error => {
                    this.setState({
                        error
                    });
                }
            )
    }

    onChangePrice (e){
        e.preventDefault();
        this.setState({ ...this.state, currentPrice: e.target.value });
    }

    calculate (e, stock, operation){
        e.preventDefault();

        let stocks = this.state.stocks, stockIndex = -1, updatedStock, result;

        //Find index of selected stock
        stockIndex = stocks.findIndex(elem => elem.id === stock.id);

        //Copy stock to mantain immutability
        updatedStock = { ...stock, price: this.state.currentPrice };
        
        //Calculate value based on entered price
        switch (operation) {
            case 'dividendYield':
                result = calculateDividendYield(updatedStock);
                break;
            case 'PERatio':
                result = calculatePERatio(updatedStock);
                break;
            default:
                result = 0;
                break;
        }
        
        //Update calculated field
        updatedStock[operation] = result;

        //Update copy of stocks array
        stocks.splice(stockIndex, 1, updatedStock);

        //Set new state with updated copy array
        this.setState(stocks);
    }
    
    getVolumeWeightedStockPrice(e, stock){
        e.preventDefault();
        TradeApi.getAllTradesFromStock(stock.id)
            .then(trades => {
                let stocks = this.state.stocks, stockIndex = -1, updatedStock, result;
                result = calculateVolumeWeightedStockPrice(trades);
                stockIndex = stocks.findIndex(elem => elem.id === stock.id);
                //If there are trades in the last 5 minutes
                if (trades.length > 0) {
                    updatedStock = { ...stock, VWSP: result };
                    stocks.splice(stockIndex, 1, updatedStock);
                    this.setState({ stocks });
                } else {
                    updatedStock = { ...stock, VWSP: 0 };
                    stocks.splice(stockIndex, 1, updatedStock);
                    this.setState({ stocks });
                }
            })
            .catch(error => { throw new Error(error) });
    }
    
    //Using geometric mean of the vwsp for all stocks
    getAllShareIndex (e, stock){
        e.preventDefault();
        const asi = calculateAllShareIndex(stock);
        this.setState({ result: asi });
    }

    render() {
        let stocks = this.state.stocks;

        if (!this.state.loaded) {
            return null
        } else {
            return (
                <div className="stocks-ctnr">
                    <h2>Global Beverage Corporation Exchange Stocks</h2>
                    <div className="price-ctnr">
                        <label htmlFor={name}>Stock Price</label>
                        <input className="stock__price" type="text" onChange={this.onChangePrice} placeholder="Enter price" />
                    </div>
                    <ul className="stock__list">
                        {stocks.map(stock =>
                            <li className="stock__elem" key={stock.id} data-name={stock.symbol}>
                                <form>
                                    <div className="metrics-ctnr">
                                        <span className="stock__metric">Dividend Yield: {stock.dividendYield}</span>
                                        <button className="stock__btn" onClick={e=>this.calculate(e, stock, 'dividendYield')}>Get Dividend Yield</button>

                                        <span className="stock__metric">PE Ratio: {stock.PERatio}</span>
                                        <button className="stock__btn" onClick={e => this.calculate(e, stock, 'PERatio')}>Get PE Ratio</button>

                                        <span className="stock__metric">VWSP: {stock.VWSP}</span>
                                        <button className="stock__btn" onClick={e => this.getVolumeWeightedStockPrice(e, stock)}>Get Volume Weighted Stock Price</button>
                                    </div>
                                </form>
                            </li>
                        )}
                    </ul>
                </div>
            );
        }
    }
}
