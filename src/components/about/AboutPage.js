import React from 'react';

// Since this component is simple and static, there's no parent container for it.
const AboutPage = () => {
    return (
        <div className="about">
            <h2>About</h2>
            <p>
                This assigment was develop based on a boilerplate code that can be found in <a target="blank" href="https://github.com/coryhouse/react-slingshot">React-Slingshot starter kit</a> from Corey House.
            </p>
            <h3>Specifications for the project include</h3>
            <h4>Requirements</h4>
            <ol>
                <li>
                    The Global Beverage Corporation Exchange is a new stock market trading in drinks companies.
                    <ul>
                        <li>Your company is building the object-oriented system to run that trading.</li>
                        <li>You have been assigned to build part of the core object model for a limited phase 1</li>
                    </ul>
                </li>
                <li>
                    Provide the complete source code that will:
                    <h5>For a given stock</h5>
                    <ul>
                        <li>Given any price as input, calculate the dividend yield</li>
                        <li>Given any price as input, calculate the P/E Ratio</li>
                        <li>Record a trade, with timestamp, quantity, buy or sell indicator and price</li>
                        <li>Calculate Volume Weighted Stock Price based on trades in past 5 minutes</li>
                    </ul>
                     <h5>Calculate the GBCE All Share Index using the geometric mean of the Volume Weighted Stock Price for all stocks</h5>
                </li>
            </ol>
            <h4>Constraints & Notes</h4>
            <ol>
                <li>Written in Javascript.</li>
                <li>The source code should be suitable for forming part of the object model of a production application, and can be proven to meet the requirements. A shell script is not an appropriate submission for this assignment. </li>
                <li>No database is required, all data need only be held in memory</li>
                <li>No prior knowledge of stock markets or trading is required – all formulas are provided below.</li>
                <li>The code should provide only the functionality requested, however it must be production quality.</li>
                <li> A GUI is required. Use React.js</li>
            </ol>
            <small>Observations: <br/> 
                <ul>
                    <li>delay.js contains the time (in milliseconds) for the mock API to respond</li>
                    <li>Unit tests are missing</li>
                    <li>All Share Index functionality is missing</li>
                </ul>
            </small>
        </div>
    );
};

export default AboutPage;
