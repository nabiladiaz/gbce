/* eslint-disable import/no-named-as-default */
import { NavLink, Route, Switch } from "react-router-dom";
import AboutPage from "./about/AboutPage";
import HomePage from "./home/HomePage";
import StockPage from "./stock/StockPage";
import TradePage from "./trades/TradePage";
import NotFoundPage from "./NotFoundPage";
import PropTypes from "prop-types";
import React from "react";
import { hot } from "react-hot-loader";

// This is a class-based component because the current
// version of hot reloading won't hot reload a stateless
// component at the top-level.

class App extends React.Component {
    render() {
        const activeStyle = { color: '#2B5AAC' };
        return (
            <div>
                <div className="navbar">
                    <NavLink exact to="/" activeStyle={activeStyle}>Home</NavLink>
                    {' | '}
                    <NavLink to="/stocks" activeStyle={activeStyle}>GBCE Stocks</NavLink>
                    {' | '}
                    <NavLink to="/trade" activeStyle={activeStyle}>Trades</NavLink>
                    {' | '}
                    <NavLink to="/about" activeStyle={activeStyle}>About</NavLink>
                </div>
                <Switch>
                    <Route exact path="/" component={HomePage} />
                    <Route path="/stocks" component={StockPage} />
                    <Route path="/trade" component={TradePage} />
                    <Route path="/about" component={AboutPage} />
                    <Route component={NotFoundPage} />
                </Switch>
            </div>
        );
    }
}

App.propTypes = {
    children: PropTypes.element
};

export default hot(module)(App);
