//Operations
export function calculateDividendYield({ price, type, lastDividend, parValue, fixedDividend }) {
    if (price === 0) {
        return 0;
    }
    return type === 'common' ? lastDividend / price : fixedDividend * parValue / price;
}

export function calculatePERatio({ price, lastDividend }) {
    if (lastDividend === 0) {
        return 0;
    }
    return price / lastDividend;
}

export function calculateVolumeWeightedStockPrice(trades) {
    if (trades.length === 0) {
        return 0;
    }
    return trades.reduce((accu, current) => accu + current.price * current.quantity, 0) / trades.reduce((accu, current) => accu + current.quantity, 0);
}

export function calculateGeometricMean(trades) {
    if (trades.length === 0) {
        return 0;
    }
    return Math.pow(trades.reduce((accu, current) => accu * current, 1), 1 / trades.length);
}

export function calculateAllShareIndex(stocksVWSP) {
    if (stocksVWSP.length === 0) {
        return 0;
    }
    return calculateGeometricMean(stocksVWSP);
}
