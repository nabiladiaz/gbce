import delay from './delay';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
const trades = [
    {
        id: 1,
        stockName: 'ALE',
        stockId: 3,
        timestamp: 987654321,
        quantity: 50,
        type: "sell",
        price: 550
    },
    {
        id: 2,
        stockName: 'POP',
        stockId: 2,
        timestamp: 987654321,
        quantity: 10,
        type: "buy",
        price: 1000
    }
];

const generateId = () => {
    return Math.floor(Math.random()*999999);
};

class TradeApi {
    static getAllTrades() {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(trades);
            }, delay);
        });
    }

    static getAllTradesFromStock(stockId) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(trades.filter(trade => {
                    const now = new Date();
                    let diff = now - trade.timestamp;
                    //Return trades from the past 5 minutes
                    if (Math.floor((diff / 1000) / 60) < 5 && trade.stockId === stockId){
                         return trade;
                    }
                }));
            }, delay);
        });
    }

    static saveTrade(trade) {
        trade = Object.assign({}, trade);
        return new Promise((resolve) => {
            setTimeout(() => {

                if (trade.id) {
                    const existingTradeIndex = trades.findIndex(a => a.id == trade.id);
                    trades.splice(existingTradeIndex, 1, trade);
                } else {
                    trade.id = generateId(trade);
                    trades.push(trade);
                }

                resolve(trade);
            }, delay);
        });
    }
}

export default TradeApi;