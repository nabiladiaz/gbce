import delay from './delay';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
const stocks = [
    {
        "id": 2,
        "symbol": "POP",
        "type": "common",
        "price": 100,
        "lastDividend": 8,
        "fixedDividend": 0,
        "parValue": 100,
        "dividendYield": 0,
        "PERatio": 0,
        "VWSP": 0,
        "quantity": 1500
    },
    {
        "id": 3,
        "symbol": "ALE",
        "type": "common",
        "price": 200,
        "lastDividend": 23,
        "fixedDividend": 0,
        "parValue": 60,
        "dividendYield": 0,
        "PERatio": 0,
        "VWSP": 0,
        "quantity": 500
    },
    {
        "id": 4,
        "symbol": "GIN",
        "type": "preferred",
        "price": 300,
        "lastDividend": 8,
        "fixedDividend": 2,
        "parValue": 100,
        "dividendYield": 0,
        "PERatio": 0,
        "VWSP": 0,
        "quantity": 1700
    },
    {
        "id": 5,
        "symbol": "JOE",
        "type": "common",
        "price": 500,
        "lastDividend": 13,
        "fixedDividend": 0,
        "parValue": 100,
        "dividendYield": 0,
        "PERatio": 0,
        "VWSP": 0,
        "quantity": 5000
    },
    {
        "id": 1,
        "symbol": "TEA",
        "type": "common",
        "price": 150,
        "lastDividend": 0,
        "fixedDividend": 0,
        "parValue": 100,
        "dividendYield": 0,
        "PERatio": 0,
        "VWSP": 0,
        "quantity": 3000
    },
];

//This would be performed on the server in a real app.
const generateId = () => {
    return Math.random()*999999;
};

class StockApi {
    static getAllStocks() {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(stocks);
            }, delay);
        });
    }

    static saveStock(stock) {
        stock = Object.assign({}, stock); // to avoid manipulating object passed in.
        return new Promise((resolve) => {
            setTimeout(() => {

                if (stock.id) {
                    const existingStockIndex = stocks.findIndex(a => a.id == stock.id);
                    stocks.splice(existingStockIndex, 1, stock);
                } else {
                    //Just simulating creation here.
                    //The server would generate ids for new stocks in a real app.
                    //Cloning so copy returned is passed by value rather than by reference.
                    stock.id = generateId();
                    stocks.push(stock);
                }

                resolve(stock);
            }, delay);
        });
    }

    static updateStockQuantity(stock) {
        return new Promise((resolve, reject) => {
            let updatedStock, currentStock, existingStockIndex;
            
            setTimeout(() => {

                if (stock.id) {
                    existingStockIndex = stocks.findIndex(a => a.id == stock.stockId);
                    currentStock = Object.assign({}, stocks[existingStockIndex]);
                    if (currentStock.quantity >= stock.quantity){
                        currentStock.quantity -= stock.quantity;
                        updatedStock = Object.assign({}, currentStock);
                        stocks.splice(existingStockIndex, 1, updatedStock);
                    } else {
                        reject(`Not enough stocks for the trade`);
                    }
                }
                resolve(updatedStock);
            }, delay);
        });
    }

    static deleteStock(stockId) {
        return new Promise((resolve) => {
            setTimeout(() => {
                const indexOfAuthorToDelete = stocks.findIndex(stock => {
                    stock.id == stockId;
                });
                stocks.splice(indexOfAuthorToDelete, 1);
                resolve(`Stock removed!`);
            }, delay);
        });
    }
}

export default StockApi;